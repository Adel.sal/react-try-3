import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');

  
  //  creates a contact
  //   @param {object} props an object with keys `name` and `email`
  //  @returns {number} the id of the created contact (or an error if things went wrong) 
   
  const createmagnet = async (props) => {
    if(!props || !props.strength || !props.polarity || !props.shape){
      throw new Error(`you must provide a stength and polarity and shape`)
    }
    const { strength, polarity, shape } = props
    try{
      const result = await db.run(SQL`INSERT INTO magnets (strength,polarity,shape) VALUES (${strength}, ${polarity}, ${shape})`);
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }

  // /**
  //  * deletes a contact
  //  * @param {number} id the id of the contact to delete
  //  * @returns {boolean} `true` if the contact was deleted, an error otherwise 
  //  */

  const deletemagnets = async (id) => {
    try{
      const result = await db.run(SQL`DELETE FROM magnets WHERE magnets_id = ${id}`);
      if(result.stmt.changes === 0){
        throw new Error(`magnet "${id}" does not exist`)
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the magnet "${id}": `+e.message)
    }
  }

// /**
//    * Edits a contact
//    * @param {number} id the id of the contact to edit
//    * @param {object} props an object with at least one of `name` or `email`
//    */
  const updatemagnet = async (id, props) => {
    if (!props || !(props.strength || props.polarity || props.shape)) {
      throw new Error(`you must provide magnates info`);
    }
    const { strength, polarity, shape } = props;
    try {
      let statement = "";
      if (strength && polarity && shape) {
        statement = SQL`UPDATE magnets SET polarity=${polarity}, strength=${strength} WHERE magnets_id = ${id}`;
      } else if (strength) {
        statement = SQL`UPDATE magnets SET strength=${strength} WHERE magnets_id = ${id}`;
      } else if (polarity) {
        statement = SQL`UPDATE magnets SET polarity=${polarity} WHERE magnets_id = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the magnet ${id}: ` + e.message);
    }
  }

  //   /**
  //  * Retrieves a contact
  //  * @param {number} id the id of the contact
  //  * @returns {object} an object with `name`, `email`, and `id`, representing a contact, or an error 
  //  */


  const getmagnet = async (id) => {
    try{
      const magnetsList = await db.all(SQL`SELECT magnets_id AS id, strength, polarity, shape FROM magnets WHERE magnets_id = ${id}`);
      const magnet = magnetsList[0]
      if(!magnet){
        throw new Error(`magnete ${id} not found`)
      }
      return magnet
    }catch(e){
      throw new Error(`couldn't get the magnete ${id}: `+e.message)
    }
  }


  //   /**
  //  * retrieves the contacts from the database
  //  * @param {string} orderBy an optional string that is either "name" or "email"
  //  * @returns {array} the list of contacts
  //  */
  const getmagnetsList = async (orderBy) => {
    try{
      
      let statement = `SELECT magnets_id AS id, strength, polarity, shape FROM magnets`
      switch(orderBy){
        case 'strength': statement+= ` ORDER BY strength`; break;
        case 'polaity': statement+= ` ORDER BY polarity`; break;
        case 'shape': statement+= ` ORDER BY shape`; break;

        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve magnets: `+e.message)
    }
  }
  
  const controller = {
    createmagnet,
    deletemagnets,
    updatemagnet,
    getmagnet,
    getmagnetsList
  }

  return controller
}



  
  

  


  




  



export default initializeDatabase


    


























//   /**
//    * Create the table
//    **/ 
//   await db.run(`CREATE TABLE magnets (magnets_id INTEGER PRIMARY KEY AUTOINCREMENT, strength INTEGER NOT NULL, polarity text NOT NULL, shape text NOT NULL );`);

//   /**
//    * let's insert a bit of data in it. We're going to insert 10 users
//    * We first create a "statement"
//    **/
//   const stmt = await db.prepare(SQL`INSERT INTO magnets (strength, polarity, shape ) VALUES (?, ?, ?)`);
//   let i = 0;
//   while(i<10){
//     await stmt.run(` ${i}`,`positive`,`horseshoe${i}`);
//     i++
//   }
//   /** finally, we close the statement **/
//   await stmt.finalize();

//   /**
//    * Then, let's read this data and display it to make sure everything works
//    **/
  // const rows = await db.all("SELECT magnets_id AS id, strength, polarity, shape FROM magnets")
  // rows.forEach( ({ id, strength, polarity, shape }) => console.log(`[id:${id}] - ${strength} - ${polarity} - ${shape}`) )
// }

// export default { test }
