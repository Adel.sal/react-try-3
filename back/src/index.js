import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get('/magnets/list', async (req, res) => {
    const magnets_list = await controller.getmagnetsList()
    res.json(magnets_list)
  })


    // CREATE

  app.get('/magnets/new', async (req, res, next) => {
    const { strength, polarity, shape } = req.query
    const result = await controller.createmagnet({strength,polarity,shape})
    res.json({success:true, result})
  })

    // READ
    app.get('/magnets/get/:id', async (req, res, next) => {
      const { id } = req.params
      const magnet = await controller.getmagnet(id)
      res.json({success:true, result:magnet})
    })

      // DELETE
  app.get('/magnets/delete/:id', async (req, res, next) => {
    const { id } = req.params
    const result = await controller.deletemagnets(id)
    res.json({success:true, result})
  })

    // UPDATE
    app.get('/magnets/update/:id', async (req, res, next) => {
      const { id } = req.params
      const { strength, polarity, shape} = req.query
      const result = await controller.updatemagnet(id,{strength,polarity,shape})
      res.json({success:true, result})
    })

      // LIST
  app.get('/magnets/list', async (req, res, next) => {
    const { order } = req.query
    const magnets = await controller.getmagnetsList(order)
    res.json({success:true, result:magnets})
  })
  
  // ERROR
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
  })

  

  
  

  app.listen(8080, () => console.log('server listening on port 8080'))
}
start();
